#include <doodle/doodle.hpp>
#include "mathhelper.h"
using namespace doodle;
int main(void)
{
    create_window();
    while (!is_window_closed())
    {
        update_window();
        clear_background(HexColor{ 0xBF8679FF });
        set_fill_color(HexColor{ 0x8C2B2EFF });
        // opaque and wide ellipse
        draw_ellipse(0, 0, Width * 0.8f, Height * 0.6f);
        set_fill_color(HexColor{ 0xDDD3D680 });
        // translucent circle
        draw_ellipse(0, 0, Width * 0.6f);

    }
    return 0;
}